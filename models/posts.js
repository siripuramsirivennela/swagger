const mongoose = require("mongoose");

const postSchema = new mongoose.Schema(
{
    id: { type:String},
    userId: { type:String},
    title: { type: String},
    body: { type: String},
 
  
},{
   timestamps:true
});
module.exports=mongoose.model("Post",postSchema)
